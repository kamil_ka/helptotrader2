﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TechTalk.SpecFlow.Tracing;
namespace MouseRobot.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int comissionClicked = 0;
        int comission2Clicked = 0;

        int sellFeeClicked = 0;
        int sellFee2Clicked = 0;

        public MainWindow()
        {
            InitializeComponent();
                   
            string divId = "last_last";
            HtmlAgilityPack.HtmlDocument doc2 = new HtmlWeb().Load(@"https://ru.investing.com/currencies/btc-rub", "GET");

            if (doc2 != null)
            {
                string stockprice = doc2.DocumentNode.SelectSingleNode(string.Format(".//*[@id='{0}']", divId)).InnerText;
                btcToRub.Text = stockprice;
            }
        }

        double GetDouble(string input)
        {
            var sep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var formatted = input.Replace(".", sep).Replace(".", sep);
            return Convert.ToDouble(formatted);
        }
        public class DBItem
        {
            private string model;
            private string reason;
            private string description;
            private string url;
            private int oldPrice;
            private int price;
            private DateTime dateAdded;
            private string category;
            public string Category { get { return category; } set { category = value; } }
            public string Model { get { return model; } set { model = value; } }
            public string Description { get { return description; } set { description = value; } }
            public string Reason { get { return reason; } set { reason = value; } }
            public int OldPrice { get { return oldPrice; } set { oldPrice = value; } }
            public int Price { get { return price; } set { price = value; } }
            public string Url { get { return url; } set { url = value; } }
            public DateTime DateAdded { get { return dateAdded; } set { dateAdded = value; } }
            public DBItem()
            {

            }
            public DBItem(DBItem dbi)
            {
                Category = dbi.Category;
                Model = dbi.Model;
                Description = dbi.Description;
                Reason = dbi.Reason;
                OldPrice = dbi.OldPrice;
                Url = dbi.Url;
                DateAdded = dbi.DateAdded;
            }
            public DBItem(string category, string model, string desc, string reason, int oldPrice, string url, DateTime d)
            {
                Category = category;
                Model = model;
                Description = desc;
                Reason = reason;
                OldPrice = oldPrice;
                Url = url;
                DateAdded = d;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (buyPrice.Text == "" || buyAmount.Text == "" ||sellPrice.Text == "" || sellAmount.Text == "")
            {
                MessageBox.Show("Fill in all lines, please");
                return;
            }




            double buyPriceDouble = 0, buyAmountDouble = 0, btcToRubDouble = 0, buyFeeDouble = 0;
            try
            {
                buyPriceDouble = GetDouble(buyPrice.Text);
                buyAmountDouble = GetDouble(buyAmount.Text);
                btcToRubDouble = GetDouble(btcToRub.Text);

                if (comissionClicked==0 && comission2Clicked == 0)
                {
                    MessageBox.Show("Select comission value, please");
                    return;
                }

                if (comissionClicked == 1) {
                    buyFeeDouble = GetDouble(comission.Content.ToString());
                }

                else {
                    buyFeeDouble = GetDouble(comission2.Content.ToString());
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            var buySumDouble = buyPriceDouble * buyAmountDouble;
            buySum.Content = $"{buySumDouble} BTC / {buySumDouble * btcToRubDouble} руб";


            double sellAmountDouble = buyAmountDouble * (1 - buyFeeDouble / 100);
            sellAmount.Text = sellAmountDouble.ToString();

            double sellPriceDouble = 0, sellFeeDouble;
            try
            {
                sellPriceDouble = GetDouble(sellPrice.Text);

                if (sellFeeClicked == 0 && sellFee2Clicked == 0)
                {
                    MessageBox.Show("Select comission value, please");
                    return;
                }

                if (sellFeeClicked == 1) {
                    sellFeeDouble = GetDouble(sellFee.Content.ToString());
                }
                else {
                    sellFeeDouble = GetDouble(sellFee2.Content.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            var sellSumTotal = sellPriceDouble * sellAmountDouble;
            var sellSumMinusFee = sellSumTotal * (1 - sellFeeDouble / 100);

            var profitBtc = sellSumMinusFee - buySumDouble;

            profit.Content = $"{profitBtc} BTC / {profitBtc * btcToRubDouble} руб";

            if(profitBtc>= 0)
            {
                profit.Foreground = new SolidColorBrush(Colors.Green);
            } else
            {
                profit.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void buyFee_Click(object sender, RoutedEventArgs e)
        {
            comissionClicked = 1;
            comission2Clicked = 0;

            comission2.Foreground = new SolidColorBrush(Colors.Red);

            (sender as Button).Foreground = new SolidColorBrush(Colors.Green);
        }

        private void buyFee2_Click(object sender, RoutedEventArgs e)
        {
            comissionClicked = 0;
            comission2Clicked = 1;

            comission.Foreground = new SolidColorBrush(Colors.Red);

            (sender as Button).Foreground = new SolidColorBrush(Colors.Green);
        }

        private void sellFee_Click(object sender, RoutedEventArgs e)
        {
            sellFeeClicked = 1;
            sellFee2Clicked = 0;

            sellFee2.Foreground = new SolidColorBrush(Colors.Red);

            (sender as Button).Foreground = new SolidColorBrush(Colors.Green);
        }

        private void sellFee2_Click(object sender, RoutedEventArgs e)
        {
            sellFeeClicked = 0;
            sellFee2Clicked = 1;

            sellFee.Foreground = new SolidColorBrush(Colors.Red);

            (sender as Button).Foreground = new SolidColorBrush(Colors.Green);
        }

        private void Restart_Click(object sender, RoutedEventArgs e)
        {
            buyPrice.Text = "";
            buyAmount.Text = "";
            sellPrice.Text = "";
            sellAmount.Text = "";
            buySum.Content = "";
            profit.Content = "";
        }

        private void buyPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            int element = 0;
            StringBuilder buyprice=new StringBuilder();
            string oneword=" ";
            StringBuilder buffer= new StringBuilder(Clipboard.GetText());
            int bufferLenght = buffer.Length;

            while (buffer[element]!=' ')
            {
                oneword = buffer[element].ToString();

                buyprice.Insert(element, oneword);

                element++;
                bufferLenght--;
            }
            string buyPriceString = Convert.ToString(buyprice);
            buyPrice.Text = buyPriceString;

            StringBuilder buyamount = new StringBuilder();

                while (buffer[element] == ' ')
                {
                    element++;
                bufferLenght--;
                }

            int buyAmountEl = 0;

            while (bufferLenght != 0&&buffer[element] != ' ')
            {
                oneword = buffer[element].ToString();

                buyamount.Insert(buyAmountEl, oneword);

                element++;
                buyAmountEl++;
                bufferLenght--;
            }
                
            string buyAmountString = Convert.ToString(buyamount);
            buyAmount.Text = buyAmountString;

        }

        private void sellPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            int element = 0;
            StringBuilder sellprice = new StringBuilder();
            string oneword = " ";
            StringBuilder buffer = new StringBuilder(Clipboard.GetText());
            int bufferLenght = buffer.Length;

            while (buffer[element] != ' ')
            {
                oneword = buffer[element].ToString();

                sellprice.Insert(element, oneword);

                element++;
                bufferLenght--;
            }
            string sellPriceString = Convert.ToString(sellprice);
            sellPrice.Text = sellPriceString;

            StringBuilder buyamount = new StringBuilder();

            while (buffer[element] == ' ')
            {
                element++;
                bufferLenght--;
            }

            int buyAmountEl = 0;

            while (bufferLenght != 0 && buffer[element] != ' ')
            {
                oneword = buffer[element].ToString();

                buyamount.Insert(buyAmountEl, oneword);

                element++;
                buyAmountEl++;
                bufferLenght--;
            }


            string sellAmountString = Convert.ToString(buyamount);
            sellAmount.Text = sellAmountString;
        }
    }
}
